package com.example.daoimpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.CourseDAO;
import com.example.model.CourseModel;

@Service
public class CourseDAOImpl implements CourseDAO {
	
	@Autowired
	private RestTemplate restTemplateCourseDAOImpl;
	
	@Bean
	public RestTemplate restTemplateCourseDAOImpl() {
	    return new RestTemplate();
	}

	@Override
	public CourseModel selectCourse(String idCourse) {
		CourseModel course = restTemplateCourseDAOImpl
				.getForObject("http://localhost:8080/rest/course/view/"+idCourse, CourseModel.class);
		return course;
	}

	@Override
	public List<CourseModel> selectAllCourses() {
		CourseModel[] responseEntity = restTemplateCourseDAOImpl
				.getForObject("http://localhost:8080/rest/course/viewall", CourseModel[].class); 
		return Arrays.asList(responseEntity);
	}

}
