package com.example.daoimpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.StudentDAO;
import com.example.model.StudentModel;

@Service
public class StudentDAOImpl implements StudentDAO {
	
	@Autowired
	private RestTemplate restTemplateStudentDAOImpl;
	
	@Bean
	public RestTemplate restTemplateStudentDAOImpl() {
	    return new RestTemplate();
	}

	@Override
	public StudentModel selectStudent(String npm) {
		StudentModel student = restTemplateStudentDAOImpl
				.getForObject("http://localhost:8080/rest/student/view/"+npm, StudentModel.class);
		return student;
	}

	@Override
	public List<StudentModel> selectAllStudents() {
		StudentModel[] responseEntity = restTemplateStudentDAOImpl
				.getForObject("http://localhost:8080/rest/student/viewall", StudentModel[].class); 
		return Arrays.asList(responseEntity);
	}
	
	

}
