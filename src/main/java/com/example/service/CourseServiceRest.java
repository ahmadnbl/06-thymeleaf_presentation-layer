package com.example.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.CourseDAO;
import com.example.model.CourseModel;
import com.example.model.StudentModel;

import lombok.extern.slf4j.Slf4j;

@Service
@Primary
@Slf4j
public class CourseServiceRest implements CourseService {
	
	@Autowired
	CourseDAO courseDAOImpl;

	@Override
	public CourseModel selectCourse(String courseId) {
		log.info ("REST - select course with id{}", courseId);
		return courseDAOImpl.selectCourse(courseId);
	}

	@Override
	public List<CourseModel> getAllCourse() {
		log.info ("REST - select all course");
		return courseDAOImpl.selectAllCourses();
	}

}
